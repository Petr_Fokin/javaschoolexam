package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        // TODO: Implement the logic here
        if(sourceFile == null || targetFile == null)
            throw new IllegalArgumentException();

        ArrayList<String> list = new ArrayList<String>();      //make a ArrayList

        try {

            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(sourceFile)));
            String line;
            while ((line = reader.readLine()) != null)
            {
                list.add(line);                     // read data from first file and add these in ArraysList
            }
        }
        catch (Exception a)  //catch exceptions
        {
            System.out.println("Error is " + a);                      // output information about exception
            return false;                               // and return false
        }


        Map<String, Integer> sort = new TreeMap<String, Integer>();          // make TreeMap for sort our data
        for (String b : list)
        {
            sort.put (b, Collections.frequency(list, b));    //form a sorted map, key, it'll be an element (line) from the collection list,
                                                                // and in the value indicates the number of repetitions of lines
        }




        try {
            if (targetFile.exists() && targetFile.isFile())      //check our target file (if it exists)
            {

                FileWriter writer = new FileWriter(targetFile, true);   //write data in second file after primary data
                // (use for it special constructor with boolean value - true)

                for (Map.Entry<String, Integer> entry : sort.entrySet())
                    writer.write(entry.getKey() + " " + "[" + entry.getValue() + "]" + "\r\n");   //ouput data in second file from our Map, in
                writer.close();                                                 //close stream

            } else {

                File newFile = new File("C:/newFile.txt");              // if second file doesn't exist - we create new file
                FileWriter writer = new FileWriter(newFile, true);

                for (Map.Entry<String, Integer> entry : sort.entrySet())
                    writer.write(entry.getKey() + " " + "[" + entry.getValue() + "]" + "\r\n"); //write data in second file after primary data
                writer.close();                                         //close stream

            }
            return true;
        }
        catch (Exception e)
        {
            System.out.println("Error is " + e);                                // output information about exception
            return false;                                                   // and return false
        }
    }
}
