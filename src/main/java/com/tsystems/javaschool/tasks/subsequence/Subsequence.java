package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if(x==null || y== null)
            throw new IllegalArgumentException();

        boolean meaning = true;         //make boolean variable (true), this variable will then return a value of true (the sequence is found) or false (the sequence isn't found),
                                            // as it'll help (to stop the first loop, where we "run" on the first collection)

        int a = 0;                           //make int variable, It'll store the serial number of an element from the second collection


        for (int i = 0; i < x.size(); i++)   // this loop will execute until expression until the sequence is verified
        {
            if (meaning) {
                meaning = false;             //conditions are satisfied in the variable is overwritten false, in a case where a subsequent loop should walk on the values of the second collection
                // and doesn't find a match (without considering the "erased"), meaning it won't be overwritten
            } else {
                break;
            }

            for (int b = a; b < y.size(); b++)      //loop to check conditions - the presence of a pair of the second collection, the element of the first collection
            {
                if (y.get(b).equals(x.get(i)))      //check the presence of the pair, if the pair is found, then:
                {
                    a = b + 1;                      //variable "a" is index pair-element of second collection and increment variable, then
                    //loop begins with the element that follows the matched

                    meaning = true;
                    break;

                }

            }
        }
        return meaning;                     //return true - the sequence is exist, false - the sequence is not found
    }
}
