package com.tsystems.javaschool.tasks.calculator;

import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.CopyOnWriteArrayList;
import java.math.BigDecimal;
import java.math.RoundingMode;


public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    String symbols = "+-*/()";                  //set the symbols for operations and to split a string into individual elements

    private List<String> wonder(String S)        //method which splits String (uses class "StringTokenizer")
    {
        List<String> list = new CopyOnWriteArrayList<>();       //create CopyOnWriteArrayList type collection for entering into this the values of the line, as it'll store the results of calculations

        StringTokenizer separate = new StringTokenizer(S,symbols , true);
        while(separate.hasMoreTokens())
        {
            list.add(separate.nextToken());                             //split our String
        }
        return list;
    }


    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if(statement == null)
            return null;

        statement = statement.replaceAll(" ", "");                 //delete spaces
        List<String> list = wonder(statement);          //split with support our method

        for (String s : list)                    //main loop
        {
            try {
                List<String> temporary = new CopyOnWriteArrayList<>();          //make temporary collection CopyOnWriteArrayList, throw into this collection meanings between brackets and make mathematical operation
                int start = 0;                                          //start and finish are indices brackets (open & close)
                int finish = 0;
                int volume;                                                 //len characters between the brackets
                for (int i = 0; i < list.size(); i++)                   //in this loop check brackets if we have these, then take characters between brackets and make mathematical operation
                {
                    if (list.get(i).equals("("))                        //check open bracket
                    {
                        start = i;
                    }

                    if (list.get(i).equals(")"))                        //check close bracket
                    {
                        finish = i;
                        volume = finish - start;
                        temporary.addAll(list.subList(start + 1, finish));              //put meanings between brackets into temporary collection
                        String opearationTemp = operation(temporary);               //make mathematical operation, use method "operation"
                        for (int t = 0; t <= volume; t++) {
                            list.remove(start);                                 //remove from main collection characters (brackets, math symbols and figuers) which were counted
                        }
                        list.add(start, opearationTemp);                       //add into main collection mathematic result (between math symbols previous and next operands)
                        temporary.clear();                                      //clear temp. collection
                        i = -1;
                    }
                }
            }
            catch (Exception e)
            {
                return null;            //return null (if we have exception)
            }
            try
            {
                operation(list);            //mathematic operation outside the brackets (use method operation)
            }
            catch(Exception e)
            {
                return null;         //return null (if we have exception)
            }
        }

        try
        {
            double result;
            String[] buffer1= list.get(0).split("\\.");
            if(buffer1.length==2 && buffer1[1].length()>4)
                result = new BigDecimal(Double.parseDouble(list.get(0))).setScale(4, RoundingMode.UP).doubleValue();     //rounded to 4 digits
            else
                result = Double.parseDouble(list.get(0));

            String formatOfResult = String.valueOf(result);
            String[] buffer2 = formatOfResult.split("\\.");
            if(buffer2.length==2 && buffer2[1].equals("0"))
                return buffer2[0];
            else return formatOfResult;                       //return result
        }
        catch (Exception e)
        {
            return null;          //return null if we catch exception
        }
    }


    // methods for simple mathematic operations:
    private double plus(double a, double b)       //sum
    {
        return a + b;
    }
    private double minus(double a, double b)     //subtraction
    {
        return a - b;
    }
    private double multiplication(double a, double b)   //multiplication
    {
        return a * b;
    }
    private double divide(double a, double b)     //division
    {
        if (b == 0)                             //check division by 0
        {
            return Double.parseDouble(null);
        }
        return a / b;

    }

    private String operation(List<String> list)     //method for mathematic operations with meanings from collection
    {
        for (String ss: list)           //first loop is check of multiplication and division
        {
            if (ss.equals("*") || ss.equals("/"))
            {
                for (int i = 0; i < list.size(); i++)
                {
                    Double number;                  //variable of result
                    if (list.get(i).equals("*"))    //check multiplication
                    {
                        number = multiplication(Double.parseDouble(list.get(i - 1)), Double.parseDouble(list.get(i + 1)));   //counted fist and third elements (because second is mathematical sign)
                        list.set(i, number.toString());    //add result on symbol position into collection
                        list.remove(i + 1);             //delete second number
                        list.remove(i - 1);             // delete first number
                    }
                    else if (list.get(i).equals("/"))    //check division
                    {
                        number = divide(Double.parseDouble(list.get(i - 1)), Double.parseDouble(list.get(i + 1)));
                        list.set(i, number.toString());
                        list.remove(i + 1);
                        list.remove(i - 1);
                    }
                }
            }
        }
        for (String ss: list)     //second loop is check of sum and subtraction
        {
            if (ss.equals("+") || ss.equals("-"))       //the same logic
            {
                for (int i = 0; i < list.size(); i++)
                {
                    Double number;
                    if (list.get(i).equals("+"))
                    {
                        number = plus(Double.parseDouble(list.get(i - 1)), Double.parseDouble(list.get(i + 1)));
                        list.set(i, number.toString());
                        list.remove(i + 1);
                        list.remove(i - 1);
                    }
                    else if (list.get(i).equals("-"))
                    {
                        number = minus(Double.parseDouble(list.get(i - 1)), Double.parseDouble(list.get(i + 1)));
                        list.set(i, number.toString());
                        list.remove(i + 1);
                        list.remove(i - 1);
                    }
                }
            }
        }
        return list.get(0);   //return result
    }
}
